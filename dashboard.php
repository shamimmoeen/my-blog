<?php

use App\Template;

require_once 'templates/header.php';

redirect_to_login();
?>

<div id="main" class="py-4">
	<div class="container">
		<div class="d-sm-flex justify-content-between align-items-center flex-row my-4">
			<h4 class="mb-sm-0 mb-2">
				<span class="d-flex align-items-center">
					<span class="mr-2">My Posts</span>
					<a href="add-post.php" class="btn btn-sm btn-primary">Add New</a>
				</span>
			</h4>
		</div>
	</div>

	<?php
	Template::get_instance()->load(
		'templates/flash',
		array(
			'type' => 'danger',
			'key'  => 'post_action_error'
		)
	);

	Template::get_instance()->load(
		'templates/flash',
		array(
			'type' => 'success',
			'key'  => 'post_created'
		)
	);

	Template::get_instance()->load(
		'templates/flash',
		array(
			'type' => 'success',
			'key'  => 'post_updated'
		)
	);

	Template::get_instance()->load(
		'templates/flash',
		array(
			'type' => 'success',
			'key'  => 'post_deleted'
		)
	);
	?>

	<div class="container">
		<?php
		Template::get_instance()->load(
			'templates/posts-table'
		);
		?>
	</div>
</div>

<?php require_once 'templates/footer.php'; ?>
