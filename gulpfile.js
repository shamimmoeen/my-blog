const { src, dest, watch, series } = require( 'gulp' );
const sass = require( 'gulp-sass' );
const sourcemaps = require( 'gulp-sourcemaps' );
const touch = require( 'gulp-touch-cmd' );
const autoPrefix = require( 'gulp-autoprefixer' );
const babel = require( 'gulp-babel' );
const concat = require( 'gulp-concat' );
const uglify = require( 'gulp-uglify' );
const browserSync = require( 'browser-sync' ).create();
const ggf = require( 'gulp-google-webfonts' );

function fonts() {
    return src( './fonts.list' )
        .pipe( ggf( {} ) )
        .pipe( dest( './assets/fonts' ) );
}

function css() {
    return src( './assets/css/style.scss' )
        .pipe( sourcemaps.init() )
        .pipe( sass.sync( { outputStyle: 'compressed' } ).on( 'error', sass.logError ) )
        .pipe( autoPrefix() )
        .pipe( sourcemaps.write() )
        .pipe( dest( './assets/build' ) )
        .pipe( browserSync.stream() )
        .pipe( touch() );
}

function js() {
    return src( './assets/js/**/*.js' )
        .pipe( sourcemaps.init() )
        .pipe( babel( {
            presets: [ '@babel/env' ],
        } ) )
        .pipe( concat( 'scripts.js' ) )
        .pipe( uglify() )
        .pipe( sourcemaps.write() )
        .pipe( dest( './assets/build' ) )
        .pipe( touch() );
}

function browser() {
    browserSync.init( {
        open: true,
        proxy: 'http://myshop.test/',
        files: [
            './**/*.php',
        ],
    } );

    watch( './assets/css/style.scss', css );
    watch( './assets/js/**/*.js', js ).on( 'change', browserSync.reload );
}

const build = series(
    fonts,
    css,
    js,
);

module.exports.fonts = fonts;
module.exports.css = css;
module.exports.js = js;
module.exports.build = build;
module.exports.default = browser;
