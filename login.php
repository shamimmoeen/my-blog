<?php

use App\Login;
use App\Template;

require_once 'templates/header.php';

redirect_to_dashboard();

$login = new Login();
?>

<div id="main" class="py-4">
	<?php
	Template::get_instance()->load(
		'templates/breadcrumb',
		array('title' => 'Login')
	);

	Template::get_instance()->load(
		'templates/flash',
		array(
			'type' => 'success',
			'key'  => 'user_registered'
		)
	);

	Template::get_instance()->load(
		'templates/messages',
		array('messages' => $login->getMessages())
	);
	?>

	<div class="container">
		<form method="post">
			<?php
			Template::get_instance()->loadFormField(
				'text',
				array(
					'label' => 'Email address',
					'type'  => 'email',
					'name'  => 'email',
					'id'    => 'email',
				)
			);

			Template::get_instance()->loadFormField(
				'text',
				array(
					'label' => 'Password',
					'type'  => 'password',
					'name'  => 'password',
					'id'    => 'password',
				)
			);

			Template::get_instance()->loadFormField(
				'submit',
				array(
					'name'  => 'login',
					'value' => 'Login',
				)
			);
			?>
			<div class="form-group">
				Not registered? <a href="register.php">Create an account</a>.
			</div>
		</form>
	</div>
</div>

<?php require_once 'templates/footer.php'; ?>
