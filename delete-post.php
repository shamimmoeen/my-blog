<?php

require_once 'bootstrap.php';

list($session, $post, $post_id) = user_has_permission_for_post();

if ($post->deletePost($post_id)) {
	$session->put('post_deleted', 'Post deleted successfully');
	redirect_to('dashboard.php');
} else {
	$session->put('post_action_error', 'An unknown error occurred when deleting the post');
	redirect_to('dashboard.php');
}
