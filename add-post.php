<?php

use App\AddPost;
use App\Template;

require_once 'templates/header.php';

redirect_to_login();

$addPost = new AddPost();
?>

<div id="main" class="py-4">
	<?php
	Template::get_instance()->load(
		'templates/breadcrumb',
		array('title' => 'Add Post')
	);

	Template::get_instance()->load(
		'templates/messages',
		array('messages' => $addPost->getMessages())
	);
	?>

	<div class="container">
		<form method="post">
			<?php
			Template::get_instance()->loadFormField(
				'text',
				array(
					'label' => 'Title',
					'type'  => 'text',
					'name'  => 'title',
					'id'    => 'title',
				)
			);

			Template::get_instance()->loadFormField(
				'textarea',
				array(
					'label' => 'Content',
					'name'  => 'content',
					'id'    => 'content',
				)
			);

			Template::get_instance()->loadFormField(
				'submit',
				array(
					'name'  => 'addPost',
					'value' => 'Submit',
				)
			);
			?>
		</form>
	</div>
</div>

<?php require_once 'templates/footer.php'; ?>
