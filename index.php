<?php

use App\Template;

require_once 'templates/header.php';
?>

<div id="main" class="py-4">
	<?php
	Template::get_instance()->load(
		'templates/breadcrumb',
		array('title' => 'All Posts')
	);
	?>

	<?php require_once 'templates/archive.php'; ?>
</div>

<?php require_once 'templates/footer.php'; ?>
