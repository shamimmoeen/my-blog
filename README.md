# MY BLOG #

A simple project with CRUD for my university.

### What is this for? ###

* User can create an account
* Registered user can sign in
* Registered user can create/read/update/delete posts

### How do I get set up? ###

* Create a database
* Copy .env.example file into .env
* Change database information accordingly in .env file
* Run command `composer install` to install php dependencies
* Run command `npm install` to install npm packages
* Go to _project-url/dbseed.php_ to create the required tables with some dummy data
* Run command `gulp` to open the project locally in browser

### Who do I talk to? ###

* Repo owner
