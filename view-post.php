<?php

use App\Template;

require_once 'templates/header.php';

list($post_id, $post_in_db) = post_is_valid_to_show();
?>

<div id="main" class="py-4">
	<?php
	Template::get_instance()->load(
		'templates/breadcrumb',
		array('title' => $post_in_db->title)
	);
	?>

	<div class="container">
		<?php echo nl2br($post_in_db->content); ?>
	</div>
</div>

<?php require_once 'templates/footer.php'; ?>
