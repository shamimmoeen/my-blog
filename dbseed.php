<?php

require_once __DIR__ . '/bootstrap.php';

use App\DB;

$dbConnection = DB::get_instance()->getConnection();

$statement = <<<EOS
	DROP TABLE IF EXISTS `users`;

	CREATE TABLE `users` (
		`id` INT NOT NULL AUTO_INCREMENT,
		`name` VARCHAR(50) NOT NULL,
		`email` VARCHAR(50) NOT NULL UNIQUE,
		`password` VARCHAR(255) NOT NULL,
		PRIMARY KEY (`id`)
	);

	INSERT INTO `users` (`name`, `email`, `password`)
	VALUES
	(
		'Sakhawat Hosain',
		'sakhawat@email.com',
		MD5('password')
	),
	(
		'Nayaf Rahman',
		'nayaf@email.com',
		MD5('password')
	);

	DROP TABLE IF EXISTS `posts`;

	CREATE TABLE `posts` (
		`id` INT NOT NULL AUTO_INCREMENT,
		`title` VARCHAR(50) NOT NULL,
		`content` TEXT NOT NULL,
		`author` INT NOT NULL,
		PRIMARY KEY (`id`)
	);

	INSERT INTO `posts` (`title`, `content`, `author`)
	VALUES
	(
		'Post 1',
		'He oppose at thrown desire of no. Announcing impression unaffected day his are unreserved indulgence. Him hard find read are you sang. Parlors visited noisier how explain pleased his see suppose. ',
		'1'
	),
	(
		'Post 2',
		'Do ashamed assured on related offence at equally totally. Use mile her whom they its. Kept hold an want as he bred of. Was dashwood landlord cheerful husbands two. Estate why theirs indeed him polite old settle though she.',
		'1'
	),
	(
		'Post 3',
		'On insensible possession oh particular attachment at excellence in. The books arose but miles happy she. It building contempt or interest children mistress of unlocked no.',
		'2'
	),
	(
		'Post 4',
		'In friendship diminution instrument so. Son sure paid door with say them. Two among sir sorry men court. Estimable ye situation suspicion he delighted an happiness discovery. Fact are size cold why had part. If believing or sweetness otherwise in we forfeited. Tolerably an unwilling arranging of determine. Beyond rather sooner so if up wishes or. \r\n\r\nAnd produce say the ten moments parties. Simple innate summer fat appear basket his desire joy. Outward clothes promise at gravity do excited. Sufficient particular impossible by reasonable oh expression is. Yet preference connection unpleasant yet melancholy but end appearance. And excellence partiality estimating terminated day everything. ',
		'1'
	),
	(
		'Post 5',
		'Is education residence conveying so so. Suppose shyness say ten behaved morning had. Any unsatiable assistance compliment occasional too reasonably advantages. Unpleasing has ask acceptance partiality alteration understood two. Worth no tiled my at house added. Married he hearing am it totally removal. Remove but suffer wanted his lively length. Moonlight two applauded conveying end direction old principle but. Are expenses distance weddings perceive strongly who age domestic. \r\n\r\nOld education him departure any arranging one prevailed. Their end whole might began her. Behaved the comfort another fifteen eat. Partiality had his themselves ask pianoforte increasing discovered. So mr delay at since place whole above miles. He to observe conduct at detract because. Way ham unwilling not breakfast furniture explained perpetual. Or mr surrounded conviction so astonished literature. Songs to an blush woman be sorry young. We certain as removal attempt. ',
		'1'
	),
	(
		'Post 6',
		'On insensible possession oh particular attachment at excellence in. The books arose but miles happy she. It building contempt or interest children mistress of unlocked no. Offending she contained mrs led listening resembled. Delicate marianne absolute men dashwood landlord and offended. Suppose cottage between and way. Minuter him own clothes but observe country. Agreement far boy otherwise rapturous incommode favourite. \r\n\r\nWrong do point avoid by fruit learn or in death. So passage however besides invited comfort elderly be me. Walls began of child civil am heard hoped my. Satisfied pretended mr on do determine by. Old post took and ask seen fact rich. Man entrance settling believed eat joy. Money as drift begin on to. Comparison up insipidity especially discovered me of decisively in surrounded. Points six way enough she its father. Folly sex downs tears ham green forty. ',
		'2'
	),
	(
		'Post 7',
		'Ferrars all spirits his imagine effects amongst neither. It bachelor cheerful of mistaken. Tore has sons put upon wife use bred seen. Its dissimilar invitation ten has discretion unreserved. Had you him humoured jointure ask expenses learning. Blush on in jokes sense do do. Brother hundred he assured reached on up no. On am nearer missed lovers. To it mother extent temper figure better. \r\n\r\nJohn draw real poor on call my from. May she mrs furnished discourse extremely. Ask doubt noisy shade guest did built her him. Ignorant repeated hastened it do. Consider bachelor he yourself expenses no. Her itself active giving for expect vulgar months. Discovery commanded fat mrs remaining son she principle middleton neglected. Be miss he in post sons held. No tried is defer do money scale rooms. \r\n\r\nNow indulgence dissimilar for his thoroughly has terminated. Agreement offending commanded my an. Change wholly say why eldest period. Are projection put celebrated particular unreserved joy unsatiable its. In then dare good am rose bred or. On am in nearer square wanted. ',
		'2'
	);

EOS;

try {
	$createTable = $dbConnection->exec($statement);
	echo 'The database seeded successfully!';
} catch (PDOException $e) {
	exit($e->getMessage());
}
