<?php

use App\Session;

require_once 'bootstrap.php';

$session = new Session();
$session->destroy('current_user');

redirect_to('login.php');
