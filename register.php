<?php

use App\Register;
use App\Template;

require_once 'templates/header.php';

redirect_to_dashboard();

$register = new Register();
?>

<div id="main" class="py-4">
	<?php
	Template::get_instance()->load(
		'templates/breadcrumb',
		array('title' => 'Register')
	);

	Template::get_instance()->load(
		'templates/messages',
		array('messages' => $register->getMessages())
	);
	?>

	<div class="container">
		<form method="post">
			<?php
			Template::get_instance()->loadFormField(
				'text',
				array(
					'label' => 'Name',
					'type'  => 'text',
					'name'  => 'name',
					'id'    => 'name',
				)
			);

			Template::get_instance()->loadFormField(
				'text',
				array(
					'label' => 'Email',
					'type'  => 'email',
					'name'  => 'email',
					'id'    => 'email',
					'help'  => 'We\'ll never share your email with anyone else.',
				)
			);

			Template::get_instance()->loadFormField(
				'text',
				array(
					'label' => 'Password',
					'type'  => 'password',
					'name'  => 'password',
					'id'    => 'password',
				)
			);

			Template::get_instance()->loadFormField(
				'text',
				array(
					'label' => 'Confirm password',
					'type'  => 'password',
					'name'  => 'confirmPassword',
					'id'    => 'confirmPassword',
				)
			);

			Template::get_instance()->loadFormField(
				'submit',
				array(
					'name'  => 'register',
					'value' => 'Register',
				)
			);
			?>
		</form>
	</div>
</div>

<?php require_once 'templates/footer.php'; ?>
