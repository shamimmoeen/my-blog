<?php


namespace App;


class Form
{
	const ERROR = 'danger';

	const SUCCESS = 'success';

	private $messages = array();

	public function getMessages()
	{
		return $this->messages;
	}

	public function setMessage($type, $message)
	{
		$this->messages[$type][] = $message;
	}
}
