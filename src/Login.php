<?php

namespace App;

use App\Model\User;

class Login extends Form
{
	private $user;

	public function __construct()
	{
		$this->user = new User();
		$this->registerUser();
	}

	private function registerUser()
	{
		if (!isset($_POST['login'])) {
			return;
		}

		$email = isset($_POST['email']) ? trim($_POST['email']) : '';
		$password = isset($_POST['password']) ? trim($_POST['password']) : '';

		if (!$email) {
			$this->setMessage(self::ERROR, 'Email is required');
		}

		if (!$password) {
			$this->setMessage(self::ERROR, 'Password is required');
		}

		if ($this->getMessages()) {
			return;
		}

		$userByEmail = $this->user->getUserByEmail($email);

		if (!$userByEmail || $userByEmail->password !== md5($password)) {
			$this->setMessage(self::ERROR, 'The email and password combination didn\'t work');
		} else {
			$session = new Session();
			$session->put('current_user', $userByEmail);

			redirect_to('dashboard.php');
		}
	}
}
