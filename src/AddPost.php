<?php

namespace App;

use App\Model\Post;

class AddPost extends Form
{
	private $post;

	public function __construct()
	{
		$this->post = new Post();
		$this->addPost();
	}

	private function addPost()
	{
		if (!isset($_POST['addPost'])) {
			return;
		}

		$author = get_current_user_id();

		if (!$author) {
			$this->setMessage(self::ERROR, 'Permission denied');
		}

		$title = isset($_POST['title']) ? trim($_POST['title']) : '';
		$content = isset($_POST['content']) ? trim($_POST['content']) : '';

		if (!$title) {
			$this->setMessage(self::ERROR, 'Title is required');
		}

		if ($title && strlen($title) > 40) {
			$this->setMessage(self::ERROR, 'The maximum length of title is 40 characters');
		}

		if (!$content) {
			$this->setMessage(self::ERROR, 'Content is required');
		}

		if ($this->getMessages()) {
			return;
		}

		if ($this->post->addPost($title, $content, $author)) {
			$session = new Session();
			$session->put('post_created', 'Post created successfully');

			redirect_to('dashboard.php');
		} else {
			$this->setMessage(self::ERROR, 'An unknown error occurred');
		}
	}

}
