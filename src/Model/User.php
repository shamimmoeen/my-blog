<?php

namespace App\Model;

use App\DB;
use PDO;

class User
{
	private $db;

	public function __construct()
	{
		$this->db = DB::get_instance()->getConnection();
	}

	public function getUserByEmail($email)
	{
		$statement = "
            SELECT *
            FROM `users`
            WHERE `email` = ?;
        ";

		$statement = $this->db->prepare($statement);
		$statement->execute(array($email));

		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function getUserById($id)
	{
		$statement = "
            SELECT *
            FROM `users`
            WHERE `id` = ?;
        ";

		$statement = $this->db->prepare($statement);
		$statement->execute(array($id));

		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function registerUser($name, $email, $password)
	{
		$statement = "
			INSERT INTO `users`
			(`name`, `email`, `password`)
			VALUES (:name, :email, :password);
		";

		$statement = $this->db->prepare($statement);

		return $statement->execute(array(
			'name'     => $name,
			'email'    => $email,
			'password' => md5($password),
		));
	}
}
