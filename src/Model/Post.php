<?php

namespace App\Model;

use App\DB;
use PDO;

class Post
{
	private $db;

	public function __construct()
	{
		$this->db = DB::get_instance()->getConnection();
	}

	public function getPost($id)
	{
		$statement = "
            SELECT *
            FROM `posts`
            WHERE `id` = ?;
        ";

		$statement = $this->db->prepare($statement);
		$statement->execute(array($id));

		return $statement->fetch(PDO::FETCH_OBJ);
	}

	public function getPostsByAuthor($author)
	{
		$statement = "
            SELECT *
            FROM `posts`
            WHERE `author` = ?
			ORDER BY `id` DESC;
        ";

		$statement = $this->db->prepare($statement);
		$statement->execute(array($author));

		return $statement->fetchAll(PDO::FETCH_OBJ);
	}

	public function getAllPosts()
	{
		$statement = "
            SELECT *
            FROM `posts`
			ORDER BY `id` DESC
        ";

		$statement = $this->db->query($statement);

		return $statement->fetchAll(PDO::FETCH_OBJ);
	}

	public function addPost($title, $content, $author)
	{
		$statement = "
			INSERT INTO `posts`
			(`title`, `content`, `author`)
			VALUES (:title, :content, :author);
		";

		$statement = $this->db->prepare($statement);

		return $statement->execute(array(
			'title'   => $title,
			'content' => $content,
			'author'  => $author,
		));
	}

	public function deletePost($id)
	{
		$statement = "
			DELETE FROM `posts`
			WHERE `id` = :id
		";

		$statement = $this->db->prepare($statement);

		return $statement->execute(array(
			'id' => $id
		));
	}

	public function updatePost($title, $content, $id)
	{
		$statement = "
            UPDATE `posts`
            SET
                title = :title,
                content = :content
            WHERE id = :id;
        ";

		$statement = $this->db->prepare($statement);

		return $statement->execute(array(
			'title'   => $title,
			'content' => $content,
			'id'      => (int)$id,
		));
	}
}
