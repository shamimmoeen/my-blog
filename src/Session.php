<?php


namespace App;


class Session
{
	public function put($key, $message)
	{
		$_SESSION[$key] = $message;
	}

	public function flash($key)
	{
		$message = $this->get($key);
		$this->destroy($key);

		return $message;
	}

	public function get($key)
	{
		return isset($_SESSION[$key]) ? $_SESSION[$key] : '';
	}

	public function destroy($key)
	{
		if (isset($_SESSION[$key])) {
			unset($_SESSION[$key]);
		}
	}
}
