<?php

namespace App;

use App\Model\Post;

class EditPost extends Form
{
	private $post;

	public function __construct()
	{
		$this->post = new Post();
		$this->editPost();
	}

	private function editPost()
	{
		if (!isset($_POST['editPost'])) {
			return;
		}

		$author = get_current_user_id();

		if (!$author) {
			$this->setMessage(self::ERROR, 'Permission denied');
		}

		$postId = isset($_POST['postId']) ? (int)$_POST['postId'] : '';
		$title = isset($_POST['title']) ? trim($_POST['title']) : '';
		$content = isset($_POST['content']) ? trim($_POST['content']) : '';

		if (!$postId) {
			$this->setMessage(self::ERROR, 'Post id is required');
		}

		if (!$title) {
			$this->setMessage(self::ERROR, 'Title is required');
		}

		if ($title && strlen($title) > 40) {
			$this->setMessage(self::ERROR, 'The maximum length of title is 40 characters');
		}

		if (!$content) {
			$this->setMessage(self::ERROR, 'Content is required');
		}

		if ($this->getMessages()) {
			return;
		}

		$post_in_db = (new Post())->getPost($postId);

		if (!$post_in_db) {
			$this->setMessage(self::ERROR, 'Invalid post id');
		}

		if ($author !== $post_in_db->author) {
			$this->setMessage(self::ERROR, 'Permission denied');
		}

		if ($this->getMessages()) {
			return;
		}

		if ($this->post->updatePost($title, $content, $postId)) {
			$session = new Session();
			$session->put('post_created', 'Post updated successfully');

			redirect_to('dashboard.php');
		} else {
			$this->setMessage(self::ERROR, 'An unknown error occurred');
		}
	}

}
