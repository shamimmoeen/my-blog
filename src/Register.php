<?php

namespace App;

use App\Model\User;
use YoHang88\LetterAvatar\LetterAvatar;

class Register extends Form
{
	private $user;

	public function __construct()
	{
		$this->user = new User();
		$this->registerUser();
	}

	private function registerUser()
	{
		if (!isset($_POST['register'])) {
			return;
		}

		$name = isset($_POST['name']) ? trim($_POST['name']) : '';
		$email = isset($_POST['email']) ? trim($_POST['email']) : '';
		$password = isset($_POST['password']) ? trim($_POST['password']) : '';
		$confirmPassword = isset($_POST['confirmPassword']) ? trim($_POST['confirmPassword']) : '';

		if (!$name) {
			$this->setMessage(self::ERROR, 'Name is required');
		}

		if ($name && strlen($name) > 40) {
			$this->setMessage(self::ERROR, 'The maximum length of name is 40 characters');
		}

		if (!$email) {
			$this->setMessage(self::ERROR, 'Email is required');
		}

		if ($email) {
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				$this->setMessage(self::ERROR, 'Invalid email');
			} elseif (strlen($email) > 40) {
				$this->setMessage(self::ERROR, 'The maximum length of email is 40 characters');
			} elseif ($this->user->getUserByEmail($email)) {
				$this->setMessage(self::ERROR, 'The email already exists');
			}
		}

		if (!$password) {
			$this->setMessage(self::ERROR, 'Password is required');
		}

		if ($password && !$confirmPassword) {
			$this->setMessage(self::ERROR, 'Confirm password is required');
		}

		if ($password && $confirmPassword) {
			if (strlen($password) < 6) {
				$this->setMessage(self::ERROR, 'Password must be at least 6 characters');
			} elseif (strlen($password) > 40) {
				$this->setMessage(self::ERROR, 'The maximum length of password is 40 characters');
			} elseif ($password !== $confirmPassword) {
				$this->setMessage(self::ERROR, 'Confirm password didn\'t match');
			}
		}

		if ($this->getMessages()) {
			return;
		}

		if ($this->user->registerUser($name, $email, $password)) {
			$session = new Session();
			$session->put('user_registered', 'Your account has been created successfully!');

			$this->generateAvatar($name, $email);

			redirect_to('login.php');
		} else {
			$this->setMessage(self::ERROR, 'An unknown error occurred');
		}
	}

	private function generateAvatar($name, $email)
	{
		$avatars_dir = dirname(__DIR__) . '/uploads/avatars';

		if (!is_dir($avatars_dir)) {
			mkdir($avatars_dir, 0777, true);
		}

		$avatar = new LetterAvatar($name, 'circle', 128);
		$avatar->saveAs('uploads/avatars/' . $email . '.png');
	}
}
