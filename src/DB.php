<?php

namespace App;

/**
 * Class DB
 *
 * @package App
 */
class DB
{
	private $dbConnection;

	private function __construct()
	{
		$host = getenv('DB_HOST');
		$port = getenv('DB_PORT');
		$db = getenv('DB_DATABASE');
		$user = getenv('DB_USERNAME');
		$pass = getenv('DB_PASSWORD');

		try {
			$this->dbConnection = new \PDO(
				"mysql:host=$host;port=$port;charset=utf8mb4;dbname=$db",
				$user,
				$pass
			);
		} catch (\PDOException $e) {
			echo 'Database connection failed. ' . $e->getMessage();
			exit;
		}
	}

	/**
	 * Gets the instance of the class.
	 *
	 * @return DB|null
	 */
	public static function get_instance()
	{
		// Store the instance locally to avoid private static replication.
		static $instance = null;

		if (null === $instance) {
			$instance = new DB();
		}

		return $instance;
	}

	public function getConnection()
	{
		return $this->dbConnection;
	}
}
