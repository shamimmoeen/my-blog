<?php

namespace App;

/**
 * Class Template
 *
 * @package App
 */
class Template
{
	private function __construct()
	{
	}

	/**
	 * Gets the instance of the class.
	 *
	 * @return Template|null
	 */
	public static function get_instance()
	{
		// Store the instance locally to avoid private static replication.
		static $instance = null;

		if (null === $instance) {
			$instance = new Template();
		}

		return $instance;
	}

	/**
	 * Loads the form field.
	 *
	 * @param string $slug   The template path.
	 * @param array  $params The parameters.
	 * @param bool   $render Do we render or just return the output.
	 *
	 * @return bool|string
	 */
	public function loadFormField($slug, $params = array(), $render = true)
	{
		$form_field_slug = 'templates/form-fields/' . $slug;
		$form_field_params = $this->formFieldParams($params);

		return $this->load($form_field_slug, $form_field_params, $render);
	}

	/**
	 * Prepare the form field parameters.
	 *
	 * @param array $params The parameters.
	 *
	 * @return array
	 */
	private function formFieldParams($params)
	{
		$default_params = array(
			'label'       => '',
			'name'        => '',
			'type'        => 'text',
			'id'          => '',
			'placeholder' => '',
			'help'        => '',
			'value'       => '',
		);

		$merged = array_merge($default_params, $params);

		if (isset($_POST[$params['name']])) {
			$merged['value'] = $_POST[$params['name']];
		}

		if (isset($_POST[$params['name']])) {
			$merged['value'] = $_POST[$params['name']];
		}

		if ('password' === $merged['type']) {
			$merged['value'] = '';
		}

		return $merged;
	}

	/**
	 * Loads the template.
	 *
	 * @param string $slug   The template path.
	 * @param array  $params The parameters.
	 * @param bool   $render Do we render or just return the output.
	 *
	 * @return bool|string
	 */
	public function load($slug, $params = array(), $render = true)
	{
		$html = '';
		$template = $slug . '.php';

		// Loads the template.
		if (file_exists($template)) {
			extract($params, EXTR_SKIP);

			if ($render) {
				require $template;
			} else {
				ob_start();
				require $template;
				$html = ob_get_clean();
			}
		}

		return $html;
	}
}
