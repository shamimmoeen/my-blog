<?php

use App\EditPost;
use App\Template;

require_once 'templates/header.php';

redirect_to_login();

$editPost = new EditPost();

list($session, $post, $post_id, $post_in_db) = user_has_permission_for_post();
?>

<div id="main" class="py-4">
	<?php
	Template::get_instance()->load(
		'templates/breadcrumb',
		array('title' => 'Edit Post')
	);

	Template::get_instance()->load(
		'templates/messages',
		array('messages' => $editPost->getMessages())
	);
	?>

	<div class="container">
		<form method="post">
			<?php
			Template::get_instance()->loadFormField(
				'text',
				array(
					'label' => 'Title',
					'type'  => 'text',
					'name'  => 'title',
					'id'    => 'title',
					'value' => $post_in_db->title,
				)
			);

			Template::get_instance()->loadFormField(
				'textarea',
				array(
					'label' => 'Content',
					'name'  => 'content',
					'id'    => 'content',
					'value' => $post_in_db->content,
				)
			);

			Template::get_instance()->loadFormField(
				'submit',
				array(
					'name'  => 'editPost',
					'value' => 'Submit',
				)
			);
			?>
			<input type="hidden" name="postId" value="<?php echo $post_id; ?>">
		</form>
	</div>
</div>

<?php require_once 'templates/footer.php'; ?>
