<?php
/**
 * @var $messages
 */

if (!$messages) {
	return;
}
?>

<div class="container">
	<?php foreach ($messages as $type => $items): ?>
		<div class="alert alert-<?php echo $type; ?>" role="alert">
			<?php
			if (!$items) {
				continue;
			}

			echo '<ul class="m-0 pl-3">';

			foreach ($items as $item) {
				echo '<li>' . $item . '</li>';
			}

			echo '</ul>';
			?>
		</div>
	<?php endforeach; ?>
</div>
