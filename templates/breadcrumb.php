<?php
/**
 * The page title.
 *
 * @var $title
 */
?>
<div class="container">
	<div class="d-sm-flex justify-content-between align-items-center flex-row my-4">
		<h4 class="mb-sm-0 mb-2"><?php echo $title; ?></h4>
	</div>
</div>
