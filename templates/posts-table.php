<?php

use App\Model\Post;
use App\Model\User;

$user = new User();
$posts = new Post();
$user_posts = $posts->getPostsByAuthor(get_current_user_id());
$increment = 0;
?>

<table class="table table-bordered">
	<thead>
		<tr>
			<th scope="col">#</th>
			<th scope="col">Title</th>
			<th scope="col">Author</th>
			<th scope="col">Action</th>
		</tr>
	</thead>
	<tbody>
		<?php if ($user_posts): ?>
			<?php foreach ($user_posts as $post): ?>
				<?php
				$increment++;
				$author = $user->getUserById($post->author);
				$id = $post->id;
				$edit_link = post_edit_link($id);
				$delete_link = post_delete_link($id);
				$view_link = post_view_link($id);
				?>
				<tr>
					<th scope="row"><?php echo $increment; ?></th>
					<td>
						<a href="<?php echo $view_link; ?>"><?php echo $post->title; ?></a>
					</td>
					<td><?php echo $author->name; ?></td>
					<td>
						<a href="<?php echo $edit_link; ?>">Edit</a>
						|
						<a href="<?php echo $delete_link; ?>">Delete</a>
					</td>
				</tr>
			<?php endforeach; ?>
		<?php else: ?>
			<tr>
				<td colspan="4">No posts found!</td>
			</tr>
		<?php endif; ?>
	</tbody>
</table>
