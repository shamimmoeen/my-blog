<?php
/**
 * @var $type
 * @var $key
 */

use App\Session;

$flash = new Session();
$message = $flash->flash($key);

if (!$message) {
	return;
}

$class = 'success' === $type ? 'success' : 'danger';
?>

<div class="container">
	<div class="alert alert-<?php echo $class; ?>">
		<?php echo $message; ?>
	</div>
</div>
