<?php
$user_id = get_current_user_id();
?>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<div class="container">
		<a class="navbar-brand" href="index.php">MY BLOG</a>
		<button
			class="navbar-toggler"
			type="button"
			data-toggle="collapse"
			data-target="#mainNavbarDropdown"
			aria-controls="mainNavbarDropdown"
			aria-expanded="false"
			aria-label="Toggle navigation"
		>
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="mainNavbarDropdown">
			<ul class="navbar-nav ml-auto">
				<?php if ($user_id): ?>
					<li class="nav-item dropdown">
						<a
							class="nav-link dropdown-toggle avatar-toggle"
							href="#"
							id="navbarDropdown"
							role="button"
							data-toggle="dropdown"
							aria-haspopup="true" aria-expanded="false"
						>
							<?php get_avatar(); ?>
						</a>
						<div
							class="dropdown-menu dropdown-menu-right"
							aria-labelledby="navbarDropdown"
						>
							<a class="dropdown-item" href="dashboard.php">Dashboard</a>
							<a class="dropdown-item" href="logout.php">Logout</a>
						</div>
					</li>
				<?php else: ?>
					<li class="nav-item">
						<a class="nav-link" href="login.php">Login</a>
					</li>
				<?php endif; ?>
			</ul>
		</div>
	</div>
</nav>
