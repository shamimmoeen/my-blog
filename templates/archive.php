<?php

use App\Model\Post;

$post = new Post();
$all_posts = $post->getAllPosts();
?>

<div class="container">
	<?php if ($all_posts): ?>
		<?php foreach ($all_posts as $item): ?>
			<?php
			$id = $item->id;
			$view_link = post_view_link($id);
			?>
			<div class="card mb-4">
				<div class="card-body">
					<h5 class="card-title"><?php echo $item->title; ?></h5>
					<p class="card-text"><?php echo limit_text($item->content, 50); ?></p>
					<a href="<?php echo $view_link; ?>" class="btn btn-primary stretched-link">View Post</a>
				</div>
			</div>
		<?php endforeach; ?>
	<?php else: ?>
		<p>No posts found!</p>
	<?php endif; ?>
</div>
