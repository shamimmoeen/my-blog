<?php
/**
 * @var $name
 * @var $value
 */
?>

<div class="form-group">
	<input
		type="submit"
		name="<?php echo $name; ?>"
		value="<?php echo $value; ?>"
		class="btn btn-primary"
	>
</div>
