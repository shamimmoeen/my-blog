<?php
/**
 * @var $label
 * @var $type
 * @var $id
 * @var $name
 * @var $value
 * @var $help
 */

$helpId = $help ? $id . 'Help' : '';
?>

<div class="form-group">
	<label for="<?php echo $id; ?>"><?php echo $label; ?></label>
	<textarea
		class="form-control"
		rows="6"
		name="<?php echo $name; ?>"
		id="<?php echo $id; ?>"
	><?php echo $value; ?></textarea>
	<?php if ($help): ?>
		<small id="<?php echo $helpId; ?>" class="form-text text-muted">
			<?php echo $help; ?>
		</small>
	<?php endif; ?>
</div>
