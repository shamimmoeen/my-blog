<?php
/**
 * @var $label
 * @var $type
 * @var $id
 * @var $name
 * @var $value
 * @var $help
 */

$helpId = $help ? $id . 'Help' : '';
?>

<div class="form-group">
	<label for="<?php echo $id; ?>"><?php echo $label; ?></label>
	<input
		class="form-control"
		type="<?php echo $type; ?>"
		id="<?php echo $id; ?>"
		name="<?php echo $name; ?>"
		value="<?php echo $value; ?>"
		<?php echo $help ? 'aria-describedby="' . $helpId . '"' : ''; ?>
	>
	<?php if ($help): ?>
		<small id="<?php echo $helpId; ?>" class="form-text text-muted">
			<?php echo $help; ?>
		</small>
	<?php endif; ?>
</div>
