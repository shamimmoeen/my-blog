	<footer class="bg-light text-muted text-center py-5">
		<?php
		$build_year = '2020';
		$current_year = date( 'Y' );
		$years = $build_year !== $current_year ? $build_year . ' - ' . $current_year : $build_year;
		?>
		Copyright &copy; <?php echo $years; ?> MY BLOG. All rights reserved.
	</footer>

	<script src="assets/lib/jquery/jquery-3.5.1.min.js"></script>
	<script src="assets/lib/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
