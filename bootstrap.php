<?php

require 'vendor/autoload.php';

use App\DB;
use Dotenv\Dotenv;

session_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$dotenv = Dotenv::createUnsafeImmutable(__DIR__);
$dotenv->load();

$dbConnection = DB::get_instance();
