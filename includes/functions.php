<?php

use App\Model\Post;
use App\Session;

function get_current_userdata()
{
	$session = new Session();

	return $session->get('current_user');
}

function get_current_user_id()
{
	$user = get_current_userdata();

	return isset($user->id) ? $user->id : 0;
}

function get_avatar($email = null)
{
	if (null === $email) {
		$user = get_current_userdata();
		$email = $user->email;
	}

	$url = '/uploads/avatars/' . $email . '.png';

	if (!file_exists(dirname(__DIR__) . $url)) {
		$url = '/assets/images/avatar-default.png';
	}

	echo '<img src="' . $url . '" class="avatar" alt="' . $email . '">';
}

function is_logged_in()
{
	return boolval(get_current_userdata());
}

function redirect_to($url)
{
	header('Location: ' . $url);
	exit();
}

function redirect_to_login()
{
	if (!get_current_user_id()) {
		redirect_to('login.php');
	}
}

function redirect_to_dashboard()
{
	if (get_current_user_id()) {
		redirect_to('dashboard.php');
	}
}

/**
 * @return array
 */
function user_has_permission_for_post()
{
	$session = new Session();
	$post = new Post();

	$user_id = get_current_user_id();
	$post_id = isset($_GET['id']) ? (int)$_GET['id'] : 0;

	if (!$post_id) {
		$session->put('post_action_error', 'Post id is required');
		redirect_to('dashboard.php');
	}

	if (!$user_id) {
		$session->put('post_action_error', 'Permission denied');
		redirect_to('dashboard.php');
	}

	$post_in_db = $post->getPost($post_id);

	if (!$post_in_db) {
		$session->put('post_action_error', 'Invalid post id');
		redirect_to('dashboard.php');
	}

	if ($user_id !== $post_in_db->author) {
		$session->put('post_action_error', 'Permission denied');
		redirect_to('dashboard.php');
	}

	return array($session, $post, $post_id, $post_in_db);
}

function post_is_valid_to_show()
{
	$post = new Post();

	$post_id = isset($_GET['id']) ? (int)$_GET['id'] : 0;

	if (!$post_id) {
		redirect_to('index.php');
	}

	$post_in_db = $post->getPost($post_id);

	if (!$post_in_db) {
		redirect_to('index.php');
	}

	return array($post_id, $post_in_db);
}

function post_edit_link($id)
{
	return 'edit-post.php?id=' . $id;
}

function post_delete_link($id)
{
	return 'delete-post.php?id=' . $id;
}

function post_view_link($id)
{
	return 'view-post.php?id=' . $id;
}

function limit_text($text, $limit)
{
	if (str_word_count($text, 0) > $limit) {
		$words = str_word_count($text, 2);
		$pos = array_keys($words);
		$text = substr($text, 0, $pos[$limit]) . '...';
	}

	return $text;
}
